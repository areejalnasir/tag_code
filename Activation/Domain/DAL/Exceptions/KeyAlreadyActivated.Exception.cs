﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DAL.Exceptions
{
    public class KeyAlreadyActivatedException : BusinessException
    {
        public KeyAlreadyActivatedException()
            : base()
        {
        }

        public KeyAlreadyActivatedException(String message)
          : base(message)
        {
        }

        public KeyAlreadyActivatedException(String message, Exception innerException)
          : base(message, innerException)
        {
        }

        protected KeyAlreadyActivatedException(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }
    }
}
