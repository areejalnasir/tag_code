﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DAL.Exceptions
{
    public class KeyNotExistException : BusinessException
    {
        public KeyNotExistException() : base()
        {
        }

        public KeyNotExistException(String message) : base(message)
        {
        }

        public KeyNotExistException(String message, Exception innerException)
          : base(message, innerException)
        {
        }

        protected KeyNotExistException(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }
    }
}
