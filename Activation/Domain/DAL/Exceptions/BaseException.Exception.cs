﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DAL.Exceptions
{
    public class BusinessException : Exception
    {
        public BusinessException()
         : base()
        {
        }

        public BusinessException(String message)
          : base(message)
        {
        }

        public BusinessException(String message, Exception innerException)
          : base(message, innerException)
        {
        }

        protected BusinessException(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }
    }
}