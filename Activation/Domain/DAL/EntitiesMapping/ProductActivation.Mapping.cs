﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL.Entities;

namespace TAG.Accountant.DAL.EntitiesMapping
{
    public class ProductActivationMapping
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductActivation>()
                   .HasMany(e => e.PrductActivationHistories)
                   .WithRequired(e => e.ProductActivation)
                   .HasForeignKey(e => e.ProductActivationId)
                   .WillCascadeOnDelete(true);

        }
    }
}
