﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Framework.Web;

namespace TAG.Accountant.DAL.Entities
{
    public class ProductActivation : BaseEntity
    {

        public int Id { get; set; }

        public string Key { get; set; }

        public ActivationStatus Status { get; set; }
    
        public int ExpiryPeriod { get; set; }

        public LicenseType Type { get; set; }

        public DateTime? ActivationDate { get; set; }

        public string MAC { get; set; }

        public string IP { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string phone { get; set; }

        public virtual ICollection<PrductActivationHistory> PrductActivationHistories { get; set; }
    }
}
