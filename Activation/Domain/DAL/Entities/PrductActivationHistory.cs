﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Framework.Web;

namespace TAG.Accountant.DAL.Entities
{
    public class PrductActivationHistory : BaseEntity
    {

        public int Id { get; set; }

        public int ProductActivationId { get; set; }

        public String Key { get; set; }
            
        public DateTime Date { get; set; }

        public Actions Action { get; set; }

        public bool IsSuccess { get; set; }

        public string MAC { get; set; }

        public string IP { get; set; }

        public string Username { get; set; }
        
        public string Email { get; set; }

        public string phone { get; set; }
        
        public virtual ProductActivation ProductActivation { get; set; }
    }
}
