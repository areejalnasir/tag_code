﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DAL
{
    public enum ActivationStatus
    {
        Active = 1,
        NotActive = 0,
    }
}
