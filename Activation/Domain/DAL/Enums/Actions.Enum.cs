﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DAL
{
    public enum Actions
    {
        Generate = 1 ,
        Activate = 2 , 
        Deactivate = 3 ,
    }
}
