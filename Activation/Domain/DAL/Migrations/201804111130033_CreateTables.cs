namespace TAG.Accountant.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PrductActivationHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductActivationId = c.Int(nullable: false),
                        Key = c.String(),
                        Date = c.DateTime(nullable: false),
                        Action = c.Int(nullable: false),
                        IsSuccess = c.Boolean(nullable: false),
                        MAC = c.String(),
                        IP = c.String(),
                        Username = c.String(),
                        Email = c.String(),
                        phone = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductActivations", t => t.ProductActivationId, cascadeDelete: true)
                .Index(t => t.ProductActivationId);
            
            CreateTable(
                "dbo.ProductActivations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        Status = c.Int(nullable: false),
                        ExpiryPeriod = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        ActivationDate = c.DateTime(),
                        MAC = c.String(),
                        IP = c.String(),
                        Username = c.String(),
                        Email = c.String(),
                        phone = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PrductActivationHistories", "ProductActivationId", "dbo.ProductActivations");
            DropIndex("dbo.PrductActivationHistories", new[] { "ProductActivationId" });
            DropTable("dbo.ProductActivations");
            DropTable("dbo.PrductActivationHistories");
        }
    }
}
