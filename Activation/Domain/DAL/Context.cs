﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL.Entities;
using TAG.Accountant.DAL.EntitiesMapping;

namespace TAG.Accountant.DAL
{
   public class AccountantActivationContext : DbContext
    {
        public AccountantActivationContext()
            : base("name=AccountingActivationDB")

        {
        }

        public AccountantActivationContext(string connStringName) : base(connStringName)
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = true;
            //Database.SetInitializer<AccountingDBContext>(new MigrateDatabaseToLatestVersion<AccountingDBContext, Configuration>());
            //Database.SetInitializer<AccountingDBContext>(new CreateDatabaseIfNotExists<AccountingDBContext>());
            //Database.SetInitializer<AccountingDBContext>(null);
        }

        public virtual DbSet<ApplicationModule> ApplicationModule { get; set; }

        public virtual DbSet<ProductActivation> ProductActivation { get; set; }

        public virtual DbSet<PrductActivationHistory> PrductActivationHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {   
            ApplicationModuleMapping.Configure(modelBuilder);
            PrductActivationHistoryMapping.Configure(modelBuilder);
            ProductActivationMapping.Configure(modelBuilder);

        }
    }
    }
