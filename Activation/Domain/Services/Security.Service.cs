﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using Tag.Accountant.Framework;
using TAG.Accountant.DAL;
using TAG.Framework.Web;

namespace TAG.Accountant.Services
{
    public class SecurityService : ServiceBase<AccountantActivationContext>
    {

        public bool Login(string UserName, string Password)
        {
            var systemPass = "adminUserPassword".GetAppSettingsValue();
            if (UserName.ToLower() == "admin" && Password == systemPass)
            {
                HttpContext.Current.Session["IsLoggedIn"] = true;
                return true;
            }
            else
            {
                return false;
            }

        }

        public void Logout()
        {
            HttpContext.Current.Session.Remove("IsLoggedIn");
        }

        public bool IsLogged()
        {
            return (bool?)HttpContext.Current.Session["IsLoggedIn"] == true;
        }
    }
}
