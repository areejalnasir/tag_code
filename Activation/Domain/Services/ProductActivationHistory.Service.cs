﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL;
using TAG.Accountant.DAL.Entities;
using TAG.Framework.Web;

namespace TAG.Accountant.Services
{
    public class ProductActivationHistoryService : Repository<PrductActivationHistory, AccountantActivationContext>
    {
        public PrductActivationHistory CreateFromProductActivation(ProductActivation productActivationRecord, Actions Action, bool response)
        {
            PrductActivationHistory Record = new PrductActivationHistory()
            {
                Action = Action,
                IsSuccess = response,

                Key = productActivationRecord.Key,
                ProductActivationId = productActivationRecord.Id,
                Date = DateTime.Now, //(DateTime)productActivation.ActivationDate,
                MAC = productActivationRecord.MAC,
                IP = productActivationRecord.IP,

                Username = productActivationRecord.Username,
                Email = productActivationRecord.Email,
                phone = productActivationRecord.phone,
           };
            Record = this.Create(Record);
            return Record;
        }
    }
}
