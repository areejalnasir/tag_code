﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL;
using TAG.Accountant.DAL.Entities;
using TAG.Accountant.DAL.Exceptions;
using TAG.Accountant.DTO;
using TAG.Framework.Web;

namespace TAG.Accountant.Services
{
    public class ProductActivationService : Repository<ProductActivation, AccountantActivationContext>
    {
        private readonly ProductActivationHistoryService _HistorySvc = new ProductActivationHistoryService();

        public void GenerateKeys(keyGenetationDTO data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                ProductActivation GeneratedKey = new ProductActivation()
                {
                    Key = Guid.NewGuid().ToString(),
                    ExpiryPeriod = data.ExpairyPeriod,
                    Status = ActivationStatus.NotActive,
                    Type = data.LicenceType,
                };

                GeneratedKey = base.Create(GeneratedKey);
                _HistorySvc.CreateFromProductActivation(GeneratedKey, Actions.Generate, true);
            }
        }

        public ProductActivation ActivateKey(ActivateProductKeyDTO data)
        {

            var Record = GetByProductKey(data.Key);
            if (Record != null)
                if (Record.Status == ActivationStatus.NotActive)
                {
                    Record.Status = ActivationStatus.Active;
                    Record.ActivationDate = DateTime.Now; //data.ActivationDate;

                    Record.IP = data.IP;
                    Record.MAC = data.MAC;

                    Record.Username = data.Username;
                    Record.Email = data.Email;
                    Record.phone = data.phone;
                    this.Save();
                    _HistorySvc.CreateFromProductActivation(Record, Actions.Activate, true);
                }
                else
                {
                    _HistorySvc.CreateFromProductActivation(Record, Actions.Activate, false);
                    throw new KeyAlreadyActivatedException();
                }
            else
            {
                throw new KeyNotExistException();
            }
            return Record;
        }

        public ProductActivation DeactivateKey(ActivateProductKeyDTO data)
        {

            var Record = GetByProductKey(data.Key);
            if (Record == null)
            {
                throw new KeyNotExistException();
            }

            if (Record.Status != ActivationStatus.Active)
            {
               //TODO : if key not already Activated Throw Exception !! 
                throw new KeyNotExistException();
            }

            Record.Status = ActivationStatus.NotActive;
            Record = base.Update(Record);
            _HistorySvc.CreateFromProductActivation(Record, Actions.Deactivate, true);

            return Record;
          
        }

        public ProductActivation GetByProductKey(string key)
        {
            //  var Result = base.GetAll().Where(e => e.Key == key).FirstOrDefault();
            return this.GetOne(e => e.Key == key); ;
        }

        public bool IsActivatedKey(string key)
        {
            var Result = this.GetOne(e => e.Key == key);
            return (Result != null && Result.Status == ActivationStatus.Active);
        }

        //TODO : implement GetAll Data
    }
}
