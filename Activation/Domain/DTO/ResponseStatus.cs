﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DTO
{
    public class ResponseStatus
    {
        public ResponseStatus(bool isSuccess , string code , string description) {
            IsSuccess = isSuccess;
            Code = code;
            Description = description;
        }

        public bool IsSuccess { get; set; }

        public String Code { get; set; }

        public String Description { get; set; }
    }
}
