﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Framework.Web;

namespace TAG.Accountant.DTO.Lookups
{
    public class ApplicationModuleDto : BaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }

    }
}
