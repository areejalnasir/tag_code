﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL;

namespace TAG.Accountant.DTO
{
    public class keyGenetationDTO
    {
        public int Count { get; set; }

        public LicenseType LicenceType { get; set; }

        public int ExpairyPeriod { get; set; }
    }
}
