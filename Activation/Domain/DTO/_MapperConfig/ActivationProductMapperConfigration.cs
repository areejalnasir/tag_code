﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Accountant.DAL.Entities;

namespace TAG.Accountant.DTO._MapperConfig
{
    class ActivationProductMapperConfigration
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ProductActivation, ProductActivationDTO>();
             
        }
    }
}
