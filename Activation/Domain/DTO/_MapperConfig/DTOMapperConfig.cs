﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DTO._MapperConfig
{
    public class DTOMapperConfig
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            AllMapperConfig.Configure(cfg);

        }

    }
}
