﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Accountant.DTO
{
    public class ActivateProductKeyDTO
    {
        public string Key { get; set; }

        public DateTime? ActivationDate { get; set; }

        public string MAC { get; set; }

        public string IP { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string phone { get; set; }
    }
}
