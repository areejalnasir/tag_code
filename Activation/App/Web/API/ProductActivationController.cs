﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Http;
using TAG.Accountant.DAL;
using TAG.Accountant.DAL.Entities;
using TAG.Accountant.DAL.Exceptions;
using TAG.Accountant.DTO;
using TAG.Accountant.DTO.SearchParams;
using TAG.Accountant.Services;
using TAG.Framework.Web;

namespace TAG.Accountant.Activation.API
{
    [RoutePrefix("Activation")]

    public class ProductActivationController : BaseRestController<ProductActivationService, ProductActivation, ProductActivationDTO, ProductActivationSearchParameter, AccountantActivationContext>
    {
        private ProductActivationService _SRV = new ProductActivationService();

        [HttpPost]
        [Route("GenerateKey")]
        public void GenerateKey([FromBody] keyGenetationDTO data)
        {
            _SRV.GenerateKeys(data);
        }


        [HttpPost]
        [Route("Activate")]
        public ResponseStatus ActivateKey([FromBody] ActivateProductKeyDTO data)
        {

            try
            {
                var Result = _SRV.ActivateKey(data);
                return new ResponseStatus(true, "ok", "Key Activation Success");
            }
            catch (KeyAlreadyActivatedException e)
            {
                return new ResponseStatus(false, "Invalid Key", "Key Already Activated");
            }
            catch (KeyNotExistException e)
            {
                return new ResponseStatus(false, "Key Not Found ", "Activate Key Fail");
            }
        }
        

        [HttpPost]
        [Route("Deactivate")]
        public ResponseStatus DeactivateKey([FromBody] ActivateProductKeyDTO data)
        {

            try
            {
                var Result = _SRV.DeactivateKey(data);
                return new ResponseStatus(true, "ok", "Key Deactivation Success");
            }
           
            catch (KeyNotExistException e)
            {
                return new ResponseStatus(false , "Key Not Found ", "Activate Key Fail");
            }  
        }
    }
}