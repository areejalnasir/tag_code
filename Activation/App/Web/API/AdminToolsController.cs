﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using TAG.Accountant.Services;
using TAG.Framework.Web;

namespace TAG.Accountant.Activation.API
{
    public class AdminToolsController : BaseAPIController
    {
        private SecurityService _Sec = new SecurityService();

        //[AllowAnonymous]
        [HttpGet]
        public void SeedSomthing()
        {
            
            if (!_Sec.IsLogged())
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
        }
    }
}