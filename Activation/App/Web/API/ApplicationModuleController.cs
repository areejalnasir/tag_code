﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TAG.Accountant.DAL;
using TAG.Accountant.DAL.Entities;
using TAG.Accountant.DTO.Lookups;
using TAG.Accountant.DTO.SearchParams;
using TAG.Accountant.Services;
using TAG.Framework.Web;

namespace TAG.Accountant.Activation.API
{
    [RoutePrefix("api/application-module")]

    public class ApplicationModuleController : BaseRestController<ApplicationModuleService, ApplicationModule, ApplicationModuleDto, ApplicationModuleSearchParameters, AccountantActivationContext>
    {
    
    }

}