﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TAG.Accountant.Services;
using TAG.Framework.Web;

namespace TAG.Accountant.Activation.API
{
    [RoutePrefix("user")]
    public class SecurityController : BaseAPIController
    {
        private SecurityService _SVC = new SecurityService();

        [AllowAnonymous]
        [HttpGet]
        [Route("Login")]
        public bool Login(string UserName, string Password)
        {
            return _SVC.Login(UserName, Password);
        }

        [HttpGet]
        [Route("Logout")]
        public void Logout()
        {
            _SVC.Logout();
        }

        [AllowAnonymous]
        [HttpGet]
        public bool IsLogged()
        {
            return _SVC.IsLogged();
        }

    }
}