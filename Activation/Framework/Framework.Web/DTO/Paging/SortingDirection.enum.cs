﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework.Web
{
    public enum SortingDirection
    {
        Ascending = 1,
        Descending = -1
    }
}
