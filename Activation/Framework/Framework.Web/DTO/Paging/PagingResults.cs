﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework.Web
{
    public class PagingResults<t>
    {
        public List<t> data { get; set; }
        public int total { get; set; }
    }
}
