﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework.Web
{
    public class PagingInputParams
    {
        public int? take { get; set; }
        public int? skip { get; set; }
        public int? page { get; set; }
        public int? pageSize { get; set; }

        public string sortField { get; set; }
        public SortingDirection? sortOrder { get; set; }

        public bool isPagingParamsEmpty()
        {
            return !this.take.HasValue && !this.skip.HasValue && !this.page.HasValue && !this.pageSize.HasValue;
        }
}


}
