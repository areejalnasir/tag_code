﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace TAG.Framework.Web
{
    
    public abstract class BaseRestController<TService, TEntity, TDTO, TSearchParams, TDBContext> : BaseAPIController 
        //ToDo: only use TService as generic without need of TEntity, when using some derived class of ServiceBase. it will already define the entity type that is using        
        where TService : Repository<TEntity, TDBContext>, new()
        where TEntity : BaseEntity
        where TDTO : BaseDto
        where TSearchParams : PagingInputParams
        where TDBContext : DbContext, new()
    {
        protected TService _Svc = new TService();
        //public ApplicationUser CurrentUser { get { return UsersService.CurrentUser; } }
        //public abstract IQueryable<TEntity> SearchQuery(IQueryable<TEntity> query, TSearchParams inputs, long? ParentID = null, long? Parent2ID = null);
        //public virtual IQueryable<TEntity> GetParetnRecordsQuery(IQueryable<TEntity> query, long ParentID) //can override in child to select query baeed on parent id
        //{
        //    return query;
        //}

        //******************************************************
        //Description: below function, will call child function (searchQuery) to build the spcific query for this spqcific entity.
        //              if any paging params are filled. the reasult will be in form of poaging result. else if no paging params sent. the result will be list of entities
        //              in case of retrieving list of details for some parent. if you provide (parentID) either as query params or with url like (parent/{ParentID}/child). it will 
        //              fill parentId input parameter to be handled inside child implmentation of function (searchQuery)
        //******************************************************
        [HttpGet]
        [Route("")]
        // public PagingResults<TDTO> GetAll([FromUri]TSearchParams inputs, long? ParentID=null)  //ToDo: Can implement GetAll with out paging  (GetAllNoPaging), note if you did not pass (take, skip) it will not do paging and bring all
        public virtual HttpResponseMessage GetAll([FromUri]TSearchParams inputs)  //ToDo: Can implement GetAll with out paging  (GetAllNoPaging), note if you did not pass (take, skip) it will not do paging and bring all
        {
            
            var query = _Svc.Search(inputs);
            //query = this.SearchQuery(query, inputs, ParentID, Parent2ID); //Call abastract to refine query
            if (inputs != null && !inputs.isPagingParamsEmpty())
            {
                var pagedData = _Svc.ApplyPaging(query, inputs.take, inputs.skip);
                var Result = new PagingResults<TDTO>()
                {
                    data = AutoMapper.Mapper.Map<List<TDTO>>(pagedData.data),
                    total = pagedData.total
                };
                //Result = OnAfterFetch_GetAllPaging(Result);
                return Request.CreateResponse(HttpStatusCode.OK, Result);
            }
            else
            {
                var Result = AutoMapper.Mapper.Map<List<TDTO>>(query);
                //Result = OnAfterFetch_GetAllAsList(Result);
                return Request.CreateResponse(HttpStatusCode.OK, Result);
            }

            //HttpResponseMessage 
            //return Request.CreateResponse(HttpStatusCode.OK, product);


        }

        //[HttpGet]
        //[Route("GetByParent")]
        //// public PagingResults<TDTO> GetAll([FromUri]TSearchParams inputs, long? ParentID=null)  //ToDo: Can implement GetAll with out paging  (GetAllNoPaging), note if you did not pass (take, skip) it will not do paging and bring all
        //public virtual HttpResponseMessage GetAll([FromUri]TSearchParams inputs)  //ToDo: Can implement GetAll with out paging  (GetAllNoPaging), note if you did not pass (take, skip) it will not do paging and bring all
        //{
        //    var query = _Svc.GetAll();
        //    //query = this.SearchQuery(query, inputs, ParentID, Parent2ID); //Call abastract to refine query
        //    if (inputs != null && !inputs.isPagingParamsEmpty())
        //    {
        //        var pagedData = _Svc.ApplyPaging(query, inputs.take, inputs.skip);
        //        var Result = new PagingResults<TDTO>()
        //        {
        //            data = AutoMapper.Mapper.Map<List<TDTO>>(pagedData.data),
        //            total = pagedData.total
        //        };
        //        //Result = OnAfterFetch_GetAllPaging(Result);
        //        return Request.CreateResponse(HttpStatusCode.OK, Result);
        //    }
        //    else
        //    {
        //        var Result = AutoMapper.Mapper.Map<List<TDTO>>(query);
        //        //Result = OnAfterFetch_GetAllAsList(Result);
        //        return Request.CreateResponse(HttpStatusCode.OK, Result);
        //    }

        //    //HttpResponseMessage 
        //    //return Request.CreateResponse(HttpStatusCode.OK, product);


        //}

        //protected virtual List<TDTO> OnAfterFetch_GetAllAsList(List<TDTO> data)
        //{
        //    return data;
        //}

        //protected virtual PagingResults<TDTO> OnAfterFetch_GetAllPaging(PagingResults<TDTO> data)
        //{
        //    return data;

        //}

        //[HttpGet]
        //[Route("")]
        //public IEnumerable<TEntity> GetParentRecords(long ParentID) //ToDo: can implement with filters and paging
        //{
        //    var Result = _Svc.GetAll();
        //    Result = this.GetParetnRecordsQuery(Result, ParentID);
        //    return Result.ToList();
        //}

        [HttpGet]
        [Route("{Id}")]
        public virtual TDTO Get(long Id)
        {
            var data = _Svc.GetById(Id);
            var Result = AutoMapper.Mapper.Map<TDTO>(data);
            return Result;
        }
        
        [HttpPost]
        [Route("")]
        public virtual TDTO Add([FromBody]TDTO inputs)
        {
            var rec = AutoMapper.Mapper.Map<TEntity>(inputs);

            TEntity data = _Svc.Create(rec, true);

            var Result = AutoMapper.Mapper.Map<TDTO>(data);
            return Result;
        }

        [HttpPut]
        [Route("{Id}")]
        public virtual TDTO Update([FromUri]long Id, [FromBody]TDTO inputs)
        {
            var rec = AutoMapper.Mapper.Map<TEntity>(inputs);

            TEntity data = _Svc.Update(rec, true);

            var Result = AutoMapper.Mapper.Map<TDTO>(data);
            return Result;
        }


        [HttpDelete]
        [Route("{Id}")]
        public virtual TDTO DELETE([FromUri]long Id)
        {
            TEntity data = _Svc.Delete(Id, true);

            var Result = AutoMapper.Mapper.Map<TDTO>(data);
            return Result;
        }

    }
}

//HttpResponseMessage 
//return Request.CreateResponse(HttpStatusCode.OK, product);