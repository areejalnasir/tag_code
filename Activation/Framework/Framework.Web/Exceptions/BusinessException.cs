﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework
{
    public class BusinessException : Exception
    {
        public BusinessException(string Message) : base(Message)
        {

        }
    }
}
