﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework
{
    public class FeatureNotEnabledException : Exception
    {
        public FeatureNotEnabledException(string message) : base(message)
        {

        }
    }
}
