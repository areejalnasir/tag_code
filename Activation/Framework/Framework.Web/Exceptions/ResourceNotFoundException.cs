﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAG.Framework
{
    public class ResourceNotFoundException : Exception
    {
        public Guid CorrespondenceId { get; set; }

        public ResourceNotFoundException(string message, Guid correspondenceId) : base(message)
        {
            this.CorrespondenceId = correspondenceId;
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
