﻿using System.Web;
using System.Web.Mvc;

namespace TAG.Framework.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TAGHandleErrorAttribute());
            //filters.Add(new WebApiAuthorizeAttribute());
            //filters.Add(new HandleErrorApiAttribute());
        }
    }
}
