﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Tag.Accountant.Framework;
using TAG.Framework;

namespace TAG.Framework.Web
{
    public class TAGHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Controller controller = filterContext.Controller as Controller;

            if (controller == null || filterContext.ExceptionHandled)
                return;

            var ex = filterContext.Exception;

            filterContext.ExceptionHandled = true;

            if (ex == null)
                return;

            if (ex.GetType() == typeof(OperationCanceledException))
                return;

            Logger.Error(ex, "Exception - " + ex.Message, new object[0]);
            if ("setting:SMTP:SendErrorEmails".GetAppSettingsValue(true)){
                //new EmailSender().SendEmailAsync("MVC Error Occure - " + "setting:environment".GetAppSettingsValue(), "setting:emailer:SuperAdminEmails".GetAppSettingsValue(), ex, "ErrorEmail.cshtml");

            }



            //ES.Framework4.Logging.Logger.Error(ex, "");

            if (ex.GetType() == typeof(HttpAntiForgeryException))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { action = "Login", controller = "Account" }));
            }
            else
            {
                //ES.Framework4.ErrorNotifier.ExceptionHandler.Handle(ex);

                if (ex.GetType() == typeof(NotAuthorizedException))
                {
                    filterContext.HttpContext.Response.StatusCode = 500;
                }
                else if (ex.GetType() == typeof(ResourceNotFoundException))
                {
                    filterContext.Result = new RedirectToRouteResult(
                                       new RouteValueDictionary
                                       {
                                           { "action", "PageNotFound" },
                                           { "controller", "StaticContent" },
                                           { "Area", "" },
                                           { "Msg", ex.GetBaseException().Message }
                                       });
                }
                //else if (ex.GetType() == typeof(FileNotFoundInDms))
                //{
                //    filterContext.Result = new RedirectToRouteResult(
                //                       new RouteValueDictionary
                //                       {
                //                           { "action", "FileNotFound" },
                //                           { "controller", "StaticContent" },
                //                           { "Area", "" },
                //                           { "Msg", ex.GetBaseException().Message }
                //                       });
                //}
                else
                {
                    var controllerName = filterContext.RouteData.Values["controller"];
                    var actionName = filterContext.RouteData.Values["action"];

                    var controllerNameStr = controllerName?.ToString();
                    var actionNameStr = actionName?.ToString();

                    filterContext.Result = new ViewResult()
                    {
                        ViewName = "Error",
                        ViewData = new ViewDataDictionary()
                        {
                            { "Model", new HandleErrorInfo(ex, controllerNameStr , actionNameStr) }
                        }
                    };

                    base.OnException(filterContext);
                }
            }
        }

    }
}