﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace TAG.Framework.Web
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]

    public class WebApiAuthorizeAttribute : AuthorizeAttribute
    {
        private string[] UserProfilesRequired { get; set; }

        public WebApiAuthorizeAttribute(params object[] userProfilesRequired)
        {
            if (userProfilesRequired.Any(p => p.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("userProfilesRequired");

            this.UserProfilesRequired = userProfilesRequired.Select(p => Enum.GetName(p.GetType(), p)).ToArray();
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            bool authorized = false;

            if (this.UserProfilesRequired.Any())
            {
                foreach (var role in this.UserProfilesRequired)
                {
                    if (HttpContext.Current.User.IsInRole(role))
                    {
                        authorized = true;
                        break;
                    }
                }
            }
            else
            {
                bool AllowAnnounous = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0;


                if (AllowAnnounous || (HttpContext.Current.User != null && HttpContext.Current.User.Identity!=null && HttpContext.Current.User.Identity.IsAuthenticated))
                {
                    authorized = true;
                }
            }

            if (!authorized)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Not allowed");
                //throw new HttpResponseException(HttpStatusCode.Unauthorized);
                //actionContext.Response.StatusCode = HttpStatusCode.Unauthorized;
                return;
            }
        }


        //protected override void HandleUnauthorizedRequest(HttpActionContext ctx)
        //{
        //    if (!ctx.RequestContext.Principal.Identity.IsAuthenticated)
        //        base.HandleUnauthorizedRequest(ctx);
        //    else
        //    {
        //        // Authenticated, but not AUTHORIZED.  Return 403 instead!
        //        ctx.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
        //    }
        //}
    //}
}
}