﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using Tag.Accountant.Framework;
using TAG.Framework;


namespace TAG.Framework.Web
{
    public class HandleErrorApiAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var ex = context.Exception;

            if (ex == null)
                return;

            if (ex.GetType() == typeof(OperationCanceledException))
                return;

            Logger.Error(ex, "API Exception - " + ex.Message,new object[0]);
            if ("setting:SMTP:SendErrorEmails".GetAppSettingsValue(true))
            {
                //new EmailSender().SendEmailAsync("API Error Occure - " + "setting:environment".GetAppSettingsValue(), "setting:emailer:SuperAdminEmails".GetAppSettingsValue(), ex, "ErrorEmail.cshtml");

            }

            //ES.Framework4.ErrorNotifier.ExceptionHandler.Handle(ex);

            if (ex.GetType() == typeof(NotAuthorizedException) || ex.GetType() == typeof(ResourceNotFoundException))
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

        }
    }
}