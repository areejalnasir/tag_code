﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace TAG.Framework.Web
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]

    public class NLMSAuthorizeAttribute : AuthorizeAttribute
    {
        private string[] UserProfilesRequired { get; set; }

        public NLMSAuthorizeAttribute(params object[] userProfilesRequired)
        {
            if (userProfilesRequired.Any(p => p.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("userProfilesRequired");

            this.UserProfilesRequired = userProfilesRequired.Select(p => Enum.GetName(p.GetType(), p)).ToArray();
        }

        public override void OnAuthorization(AuthorizationContext context)
        {
            bool authorized = false;

            if (this.UserProfilesRequired.Any())
            {
                foreach (var role in this.UserProfilesRequired)
                {
                    if (HttpContext.Current.User.IsInRole(role))
                    {
                        authorized = true;
                        break;
                    }
                }
            }
            else
            {
                if (HttpContext.Current.User != null)
                {
                    authorized = true;
                }
            }

            if (!authorized)
            {
                context.Result = new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                return;
            }
        }
    }


}