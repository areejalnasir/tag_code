﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAG.Framework.Web;

namespace TAG.Framework.Web
{
    //public class Repository<TEntity, TDBContext> : ReadOnlyRepository<TEntity, TDBContext> 
    public class Repository<TEntity, TDBContext> : ReadOnlyRepository<TEntity, TDBContext>
        where TDBContext : DbContext, new()
        where TEntity : BaseEntity
    {
        public Repository()
            : base()
        {
        }

        public Repository(TDBContext context)
            : base(context)
        {
        }

        protected virtual string SequenceName { get; }

        protected long GetNewSequence()
        {
            var Result = this.Db.Database.SqlQuery<long>($"SELECT {this.SequenceName}.NEXTVAL FROM DUAL");
            return Result.FirstOrDefault();
        }

        public virtual TEntity Create(TEntity entity, bool Save = true) //, string createdBy = null)
        {

            //entity.CreatedDate = DateTime.UtcNow;
            //entity.CreatedBy = createdBy;
            TEntity Result = this.Entities.Add(entity);
            if (Save)
            {
                this.Save();
            }
            return Result;
        }

        public virtual TEntity Update(TEntity entity, bool Save = true) //, string modifiedBy = null)
        {
            TEntity Result = entity;
            //entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = modifiedBy;
            this.Entities.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            if (Save)
            {
                this.Save();
            }
            return Result;
        }

        public virtual TEntity Delete(object id, bool Save = true)
        {
            TEntity entity = this.Entities.Find(id);
            var Result = Delete(entity, Save);
            return Result;
        }

        public virtual TEntity Delete(TEntity entity, bool Save = true)
        {
            var dbSet = this.Entities;
            if (context.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            var Result = dbSet.Remove(entity);
            if (Save)
            {
                this.Save();
            }
            return Result;

        }

        public virtual void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }
        }

        public virtual Task SaveAsync()
        {
            try
            {
                return context.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                ThrowEnhancedValidationException(e);
            }

            return Task.FromResult(0);
        }

        protected virtual void ThrowEnhancedValidationException(DbEntityValidationException e)
        {
            var errorMessages = e.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

            var fullErrorMessage = string.Join("; ", errorMessages);
            var exceptionMessage = string.Concat(e.Message, " The validation errors are: ", fullErrorMessage);
            throw new DbEntityValidationException(exceptionMessage, e.EntityValidationErrors);
        }
    }

}
